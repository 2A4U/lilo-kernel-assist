#!/usr/bin/env bash

alias upk='updatelinux'

updatelinux () {
local varDate=$(date +%a-%Y-%b-%d-%T)
cd /usr/src/linux
local currentCheckout=$(git rev-parse --abbrev-ref --symbolic-full-name @{u})
echo "Updating ${currentCheckout}..." ; git fetch --depth 1

if [[ $(git rev-list HEAD...${currentCheckout} --count) != 0 ]];then #only compile if changes are found.
echo "backing up ${PWD}/.config to ../config${varDate}" ; cp .config ../config${varDate}
echo "cleaning $PWD" ; git clean -dxf #this will delete '.config' if it is not backed up.
echo -n "Resetting git to current update--${currentCheckout}..." ; git reset --hard ${currentCheckout} &>/dev/null ; echo 'done.'
cp ../config${varDate} .config && echo ".config restored."
cdkmshm
else echo 'No update detected.'
fi
}

cdkmshm () { SECONDS='0'
readarray -t -s4 -n3 threads <<< $(lscpu) ; threads=$((${threads[0]/*:}*${threads[2]/*:}))
mkdir -p /dev/shm/linux
cd /usr/src/linux
echo -n 'Copying /usr/src/linux to /dev/shm/linux...' ; \time -f "%es" tar cf - * | ( cd /dev/shm/linux ; tar xfp - )
cd /dev/shm/linux
ln -fs /usr/src/linux/.config .config
make oldconfig
[[ ! -L .config ]] && cp -Pau .config /usr/src/linux
sed -i "s@\(export.*INSTALL_PATH\) ?= /boot@\1=/boot@g" Makefile && \ #use built-in kernel script to have lilo install to /boot.
make all -j${threads} 2> /dev/shm/linux-kernel-build-error.txt && \
make modules_install && \
make install
if [[ $? = 0 ]] ; then rm -r /dev/shm/linux
echo -e "\n \e[0;32m\e[5m \xe2\x9c\x93 \e[0m $SECONDS seconds elapsed."
else echo -e "\n \e[0;31m\e[5m \e[5m\xE2\x9A\xA0 \e[0m $SECONDS seconds elapsed."
fi
}

if [[ $1 = clone ]]; then cd /usr/src
git clone --depth 1 --branch="drm-next" https://gitlab.freedesktop.org/agd5f/linux.git drm-next
ln -s drm-next linux
elif [[ $1 = update ]]; then updatelinux
elif [[ $1 = build ]]; then cdkmshm
fi
