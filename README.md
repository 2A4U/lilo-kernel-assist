# `Build AMDGPU Kernel`

A script with Linux kernel `clone`, `update` and `build` functions which uses the kernel's call to Lilo for install--not for developers as it uses 'git clean -dxf' and 'git 'reset --hard' to erase local changes.

The script uses /dev/shm/linux for the compile directory and presumes '/usr/src/linux' is a git archive or link to a git archive.

### Example Uses

As root copy alias and functions from *cdkmshm.bash* into '$HOME/*.bash_profile*' or '$HOME/*.bashrc*' and type `upk`.

Alternatively type (to update and build)
```
. ./cdkmshm.bash ; updatelinux
```

or (to clone)
```
./cdkmshm.bash clone
```
or (to build)
```
./cdkmshm.bash build
```
